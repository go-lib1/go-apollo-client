package apollo

import (
	"flag"
	"github.com/gogf/gf/v2/text/gstr"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/shima-park/agollo"
	"strings"
	"sync"
)

var (
	apolloUrl       = flag.String("APOLLO_URL", "", "APOLLO_URL")
	apolloAppId     = flag.String("APOLLO_APP_ID", "", "APOLLO_APP_ID")
	apolloSecret    = flag.String("APOLLO_SECRET", "", "APOLLO_SECRET")
	apolloNameSpace = flag.String("APOLLO_NAMESPACE", "application", "APOLLO_NAMESPACE")
)

type apolloCenter[T any] struct {
	apo     agollo.Agollo
	f       func(*T)
	errFunc func(error) bool
}

func newApolloCenter[T any](f func(*T), ef func(error) bool) *apolloCenter[T] {
	return &apolloCenter[T]{
		f:       f,
		errFunc: ef,
	}
}

func (a *apolloCenter[T]) loadApollo() {
	flag.Parse()

	if *apolloUrl == "" || *apolloAppId == "" || *apolloSecret == "" {
		panic("apollo url or appid or secret is empty")
	}

	_opts = append(_opts, agollo.PreloadNamespaces(*apolloNameSpace), agollo.AccessKey(*apolloSecret))

	apo, err := agollo.New(*apolloUrl, *apolloAppId, _opts...)
	if err != nil {
		panic(err)
	}

	a.apo = apo
	initValue := apo.GetNameSpace(*apolloNameSpace)
	if len(initValue) == 0 {
		panic("apollo init value is empty")
	}

	if err = a.mapToConfig(initValue); err != nil {
		panic(err)
	}
}

func (a *apolloCenter[T]) mapToConfig(m map[string]any) error {
	t, err := SetConfig[T](m)
	if err != nil {
		return err
	}

	a.f(t)
	return nil
}

func (a *apolloCenter[T]) stop() {
	if a.apo != nil {
		a.apo.Stop()
	}
}

func start[T any](f func(*T), ef func(error) bool) {
	if f == nil || ef == nil {
		panic("apollo callback func is nil")
	}

	a := newApolloCenter(f, ef)
	a.loadApollo()
	defer a.stop()

	errorCh := a.apo.Start()
	watchCh := a.apo.Watch()

Exit:
	for {
		select {
		case err := <-errorCh:
			if a.errFunc(err.Err) {
				break Exit
			}

		case resp := <-watchCh:
			if err := a.mapToConfig(resp.NewValue); err != nil {
				if a.errFunc(err) {
					break Exit
				}
			}
		}
	}
}

func SetConfig[T any](m map[string]any) (*T, error) {
	var c T
	err := gconv.Struct(apolloMapToJsonMap(m), &c)
	return &c, err
}

func apolloMapToJsonMap(am map[string]any) map[string]any {
	result := make(map[string]any)

	for key, value := range am {
		keys := strings.Split(key, ".")
		lastKeyIndex := len(keys) - 1
		current := result

		for i, key1 := range keys {
			if i == lastKeyIndex {
				if str, ok := value.(string); ok {
					current[key1] = gstr.Trim(str)
				} else {
					current[key1] = value
				}
			} else {
				if _, ok := current[key1]; !ok {
					current[key1] = make(map[string]any)
				}
				current = current[key1].(map[string]any)
			}
		}
	}
	return result
}

// Run 启动apollo
// f: apollo配置变更回调函数
// ef: apollo错误回调函数, 返回true则退出apollo

func Run[T any](f func(*T), ef func(error) bool) {
	once := sync.Once{}
	wg := sync.WaitGroup{}
	wg.Add(1)

	go start(func(t *T) {
		f(t)

		once.Do(func() {
			wg.Done()
		})
	}, ef)

	wg.Wait()
}

func DebugArgs(url, appId, secret, nameSpace string) {
	*apolloUrl = url
	*apolloAppId = appId
	*apolloSecret = secret
	*apolloNameSpace = nameSpace
}
