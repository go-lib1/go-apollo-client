package apollo

import (
	"github.com/shima-park/agollo"
	"time"
)

var _opts []agollo.Option

func SetOption(opts ...agollo.Option) {
	if len(_opts) > 0 {
		_opts = append(_opts, opts...)
	}
}

func Cluster(cluster string) agollo.Option {
	return agollo.Cluster(cluster)
}

func DefaultNamespace(defaultNamespace string) agollo.Option {
	return agollo.DefaultNamespace(defaultNamespace)
}

func PreloadNamespaces(namespaces ...string) agollo.Option {
	return agollo.PreloadNamespaces(namespaces...)
}

func WithApolloClient(c agollo.ApolloClient) agollo.Option {
	return agollo.WithApolloClient(c)
}

func WithLogger(l agollo.Logger) agollo.Option {
	return agollo.WithLogger(l)
}

func AutoFetchOnCacheMiss() agollo.Option {
	return agollo.AutoFetchOnCacheMiss()
}

func LongPollerInterval(i time.Duration) agollo.Option {
	return agollo.LongPollerInterval(i)
}

func EnableHeartBeat(b bool) agollo.Option {
	return agollo.EnableHeartBeat(b)
}

func HeartBeatInterval(i time.Duration) agollo.Option {
	return agollo.HeartBeatInterval(i)
}

func BackupFile(backupFile string) agollo.Option {
	return agollo.BackupFile(backupFile)
}

func FailTolerantOnBackupExists() agollo.Option {
	return agollo.FailTolerantOnBackupExists()
}

func EnableSLB(b bool) agollo.Option {
	return agollo.EnableSLB(b)
}

func WithBalancer(b agollo.Balancer) agollo.Option {
	return agollo.WithBalancer(b)
}

func ConfigServerRefreshIntervalInSecond(refreshIntervalInSecond time.Duration) agollo.Option {
	return agollo.ConfigServerRefreshIntervalInSecond(refreshIntervalInSecond)
}

func AccessKey(accessKey string) agollo.Option {
	return agollo.AccessKey(accessKey)
}

func WithClientOptions(opts ...agollo.ApolloClientOption) agollo.Option {
	return agollo.WithClientOptions(opts...)
}
