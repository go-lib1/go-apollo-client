module gitlab.com/go-lib1/go-apollo-client

go 1.20

require (
	github.com/gogf/gf/v2 v2.3.3
	github.com/shima-park/agollo v1.2.14
)

require (
	go.opentelemetry.io/otel v1.7.0 // indirect
	go.opentelemetry.io/otel/trace v1.7.0 // indirect
)
